package yuda.ichbal.app07

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class dbdata(context: Context): SQLiteOpenHelper(context,Db_name,null,Db_ver) {
    companion object{
        val Db_name= "mhs"
        val Db_ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tb_mhs = "create table mhs(nim text primary key , nama text not null,prodi text not null )"
        val ins  = "insert into mhs('nim','nama','prodi') values ('1234','dimas','mi')"
        db?.execSQL(tb_mhs)
        db?.execSQL(ins)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

}