package yuda.ichbal.app07


import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() , View.OnClickListener {
    lateinit var db: SQLiteDatabase
    lateinit var intentIntegrator: IntentIntegrator
    lateinit var adaplis: SimpleCursorAdapter
    lateinit var buil: AlertDialog.Builder
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener(this)
        buil = AlertDialog.Builder(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        getdata()
        showtrans()
        intentIntegrator = IntentIntegrator(this)
    }

    fun getdata(): SQLiteDatabase {
        db = dbdata(this).writableDatabase
        return db
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        var intenres = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intenres != null) {
            if (intenres.contents != null) {
                isi.setText(intenres.contents)
                val st = StringTokenizer(isi.text.toString(), ";", false)
                editText.setText(st.nextToken())
                editText2.setText(st.nextToken())
                editText3.setText(st.nextToken())
            } else {
                Toast.makeText(this, "Dibatalkan ", Toast.LENGTH_SHORT).show()
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View?) {


        when (v?.id) {
            R.id.button -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()

            }

            R.id.button2 -> {
                val barcodeEncoder = BarcodeEncoder()
                //          val bitmap   = barcodeEncoder.encodeBitmap(isi.text.toString(), BarcodeFormat.QR_CODE,400,400)
                var bitmap = barcodeEncoder.encodeBitmap(
                    isi.text.toString(),
                    BarcodeFormat.QR_CODE,
                    400,
                    400
                )
                imageView.setImageBitmap(bitmap)

            }
            R.id.button3 -> {
                buil.setTitle("validasi")
                    .setMessage("apakah data sudah benar ?")
                    .setPositiveButton("YA", insertbt)
                buil.show()
            }
        }
    }




    fun insert(nim : String , nama : String ,prodi : String){
        val cv : ContentValues = ContentValues()
        cv.put("nim",nim)
        cv.put("nama",nama)
        cv.put("prodi",prodi)
        db.insert("mhs",null,cv)
        showtrans()
    }

    val insertbt = DialogInterface.OnClickListener { dialog, which ->
        insert(editText.text.toString(),editText2.text.toString(),editText3.text.toString())
        editText.setText("")
        editText2.setText("")
        editText3.setText("")
    }


    fun showtrans() {

        var sql = "select nim as _id, nama ,prodi from mhs"
        val c: Cursor = db.rawQuery(sql, null)
        adaplis = SimpleCursorAdapter(
            this,
            R.layout.isi,
            c,
            arrayOf("_id", "nama", "prodi"),
            intArrayOf(R.id.nim, R.id.nama, R.id.prody),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        listdata.adapter = adaplis
    }



}
